package juego;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class PantallaBienvenida extends JFrame {

    private JPanel contentPane;
    private JFrame frame;
    private int xOrigen = 100;
    private int yOrigen = 100;
    private int ancho = 400;
    private int alto = 400;
    private int indice = -1; // Variable para almacenar la selección del usuario


    public static void main(String[] args) {
        PantallaBienvenida pantallaInicial = new PantallaBienvenida();
        pantallaInicial.iniciar();
    }

    public static void iniciar()
    {
    	PantallaBienvenida pantalla = new PantallaBienvenida();
    	pantalla.frame.setVisible(true);
    }

    public PantallaBienvenida() {
        initialize();
    	}
    
    private void initialize() {
    	frame = new JFrame("Luces Fuera");    	
    	frame.setBounds(this.xOrigen, this.yOrigen, this.ancho, this.alto);
    	frame.setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(128, 128, 192));
        contentPane.setForeground(new Color(128, 128, 192));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
  //      setContentPane(contentPane);
        contentPane.setLayout(null);
        frame.getContentPane().add(contentPane);
        
        JLabel lblSeleccionaUnValor = new JLabel("Selecciona la dificultad:");
        lblSeleccionaUnValor.setForeground(new Color(64, 0, 128));
        lblSeleccionaUnValor.setFont(new Font("Berlin Sans FB", Font.ITALIC, 15));
        lblSeleccionaUnValor.setBounds(73, 114, 150, 14);
        contentPane.add(lblSeleccionaUnValor);
        
        JComboBox dificultad = new JComboBox();
        dificultad.setBackground(new Color(128, 128, 192));
        dificultad.setForeground(new Color(64, 0, 128));
        dificultad.setModel(new DefaultComboBoxModel(new String[] {"3", "4", "5", "7"}));
        dificultad.setBounds(251, 112, 53, 22);
        contentPane.add(dificultad);
        
        crearBotonIniciar(contentPane, dificultad);
        

    }

    private void crearBotonIniciar(JPanel panel, JComboBox dificultad ) {
    	JButton btnIniciar = new JButton("Iniciar");
    	btnIniciar.setForeground(new Color(64, 0, 128));
    	btnIniciar.setBackground(new Color(128, 128, 192));
    	btnIniciar.setFont(new Font("Berlin Sans FB", Font.ITALIC, 13));
    	btnIniciar.setBorder(BorderFactory.createLineBorder(new Color(64, 0, 128)));
        btnIniciar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	indice= (int)dificultad.getSelectedIndex();//indice de la seleccion
            	if (indice != -1) {
            		int valorSeleccionado = Integer.parseInt((String) dificultad.getItemAt(indice));
                    PantallaJuego.iniciar(valorSeleccionado);
                    frame.dispose();
                } 
            }
        });
        btnIniciar.setBounds(148, 200, 89, 23);
        contentPane.add(btnIniciar);
    }
}

package juego;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Canvas;

public class PantallaSalida extends JFrame{

	 private JPanel contentPane;
	 private JFrame frame;
	 private int xOrigen = 100;
	 private int yOrigen = 100;
	 private int ancho = 400;
	 private int alto = 400;


	 public static void iniciar()
	    {
	    	PantallaSalida window = new PantallaSalida();
	        window.frame.setVisible(true);
	    }

	 public PantallaSalida() {
	        initialize();
	    }

	 private void initialize() {
	        frame = new JFrame("Luces Fuera");
	        frame.setBounds(this.xOrigen, this.yOrigen, this.ancho, this.alto);
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        frame.setLocationRelativeTo(null);
	        contentPane = new JPanel();
	        contentPane.setForeground(new Color(128, 255, 255));
	        contentPane.setBackground(new Color(128, 128, 192));
	        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	       // setContentPane(contentPane);
	        contentPane.setLayout(null);
	        frame.getContentPane().add(contentPane);
	        
	        JLabel mensajeDeGanaste = new JLabel(" ¡GANASTE! ");
	        mensajeDeGanaste.setForeground(new Color(64, 0, 128));
	        mensajeDeGanaste.setFont(new Font("Berlin Sans FB", Font.ITALIC, 21));
	        mensajeDeGanaste.setBounds(131, 43, 115, 32);
	        contentPane.add(mensajeDeGanaste);
	        
	        JButton btnNewButton = new JButton("JUGAR DE NUEVO");
	        btnNewButton.setFont(new Font("Berlin Sans FB", Font.ITALIC, 11));
	        btnNewButton.setBorder(BorderFactory.createLineBorder(new Color(64, 0, 128)));
	        btnNewButton.setHideActionText(true);
	        btnNewButton.setFocusPainted(false);
	        btnNewButton.setForeground(new Color(64, 0, 128));
	        btnNewButton.setBackground(new Color(128, 128, 192));
	        btnNewButton.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		PantallaBienvenida.iniciar();
                    frame.dispose();
	        	}
	        });
	        btnNewButton.setBounds(107, 141, 171, 48);
	        contentPane.add(btnNewButton);

}
}
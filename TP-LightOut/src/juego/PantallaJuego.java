package juego;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class PantallaJuego {

    private JFrame frame;
    private JButton[][] matrizDeBotones;
    private int xOrigen = 100;
    private int yOrigen = 100;
    private int ancho = 400;
    private int alto = 400;
    private boolean mostrarSolucion = false;
    
    public static void iniciar(int longitud)
    {
    	PantallaJuego pantalla = new PantallaJuego(longitud);
        pantalla.frame.setVisible(true);
    }

    public PantallaJuego(int longitud) {
        initialize(longitud);
    }

    private void initialize(int longitud) 
    {
        frame = new JFrame("Luces Fuera");
        frame.setBounds(this.xOrigen, this.yOrigen, this.ancho, this.alto);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        JPanel grillaDeLuces = new JPanel();
        grillaDeLuces.setLayout(new GridLayout(longitud, longitud));  
        
        generarBotones(longitud, grillaDeLuces, Juego.generarTablero(longitud));
        actualizarColores(Juego.enviarTableroActualizado());
        captarEvento(longitud);
        
        JPanel panelDeOpciones = new JPanel();
        crearBotonMostrarSolucion(panelDeOpciones);
        crearBotonReinicio(panelDeOpciones, longitud);

        frame.add(grillaDeLuces, BorderLayout.CENTER);
        frame.add(panelDeOpciones, BorderLayout.SOUTH);     
    }
    
	private void generarBotones(int longitud, JPanel panel, int[][] matrizEncendidosApagados) 
	{
        matrizDeBotones = new JButton[longitud][longitud]; 

        for (int fila = 0; fila < longitud; fila++) 
        {
            for (int columna = 0; columna < longitud; columna++) 
            {	   
                matrizDeBotones[fila][columna] = new JButton();
                panel.add(matrizDeBotones[fila][columna]);
                matrizDeBotones[fila][columna].setBorder(BorderFactory.createEtchedBorder());
            }
        }
        
	}
	
	private void actualizarColores(int[][] matrizEncendidoApagado)
	{
        for (int fila = 0; fila < matrizEncendidoApagado.length; fila++) 
        {
            for (int columna = 0; columna < matrizEncendidoApagado.length; columna++) 
            {
            	if (matrizEncendidoApagado[fila][columna] == 1)
            	{
            		matrizDeBotones[fila][columna].setBackground(new Color(128, 128, 192));
            	}
            	else
            	{
            		matrizDeBotones[fila][columna].setBackground(new Color(64, 0, 128));
            	}	
            }
        }
	}
	
	private void captarEvento(int longitud)
	{
		for (int fila = 0; fila < longitud; fila++) 
		{
            for (int columna = 0; columna < longitud; columna++) 
            {            	   
                
                final int row = fila;
                final int col = columna;             
              
            	matrizDeBotones[row][col].addActionListener(new ActionListener() 
            	{
            		public void actionPerformed(ActionEvent e) 
            		{
            			Juego.clickSobreCasilla(row, col);  
            			Juego.actualizarMovimientosGanadores(row, col);                   
            			reiniciarBordes();
            			
            			if (mostrarSolucion)
            			{
            				mostrarMovimientosGanadores();
            			}
	                   
            			actualizarColores(Juego.enviarTableroActualizado());   
            			estaTerminado(Juego.getEstaTerminado());
                    }
                });                 
            }
        }
	}
    


    private void crearBotonMostrarSolucion(JPanel panel)
    {
    	JButton botonMostrarSolucion = new JButton("Mostrar Solucion");
    	panel.add(botonMostrarSolucion);
        
        botonMostrarSolucion.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
	    		cambiarMostrarSolucion(true);
	        	mostrarMovimientosGanadores();            	
            }
        });
    }
    
    private void crearBotonReinicio(JPanel panel, int longitud)
    {
    	JButton reiniciarJuego = new JButton("Reiniciar");
    	panel.add(reiniciarJuego);
        
    	reiniciarJuego.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
            	Juego.hacerMovimientosAleatorios(longitud);
            	cambiarMostrarSolucion(false);    
            	reiniciarBordes();
            	actualizarColores(Juego.enviarTableroActualizado());           	
            }
        });
    }
    
    private void estaTerminado(boolean estaTerminado) {
		if(estaTerminado) {
			PantallaSalida.iniciar();
			frame.dispose();
		}
	}

	
	private void reiniciarBordes() {
		for (int fila = 0; fila < matrizDeBotones.length; fila++) {
            for (int columna = 0; columna < matrizDeBotones.length; columna++) {  
            		matrizDeBotones[fila][columna].setBorder(BorderFactory.createEtchedBorder());
            }
		}
	}
	
	private void mostrarMovimientosGanadores()
	{	
		for (Point movimiento: Juego.getMovimientosGanadores())
		{	
			int fila = movimiento.x;
			int columna = movimiento.y;
			
			matrizDeBotones[fila][columna].setBorder(BorderFactory.createLineBorder(Color.yellow));
		}
	}
	
	private void cambiarMostrarSolucion(boolean b)
	{
		mostrarSolucion = b;
	}
}


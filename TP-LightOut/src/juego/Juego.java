package juego;

import java.awt.Point;
import java.util.HashSet;

public class Juego {
	
	private static int[][] matriz;
	private static HashSet<Point> movimientosGanadores;
	private static boolean estaTerminado;
	
	public static int [][] generarTablero(int longitud) 
	{
		setEstaTerminado(false);	
		hacerMovimientosAleatorios(longitud);
		
		return matriz;
	}
	
	public static void hacerMovimientosAleatorios(int longitud) 
	{
		matriz = new int[longitud][longitud];
		movimientosGanadores = new HashSet<>();
		
		for (int fila = 0; fila < matriz.length; fila++)
		{
			int aleatorio = (int) (Math.random()*(matriz.length));
			clickSobreCasilla(fila, aleatorio);
			actualizarMovimientosGanadores(fila, aleatorio);
		}
	}
	
	public static void actualizarMovimientosGanadores(int fila, int columna)
	{
		Point movimiento = new Point(fila, columna);
		
		if (esCasillaSolucion(movimiento.x, movimiento.y))
		{
			quitarDeCasillasGanadoras(movimiento);	
		}
		else
		{
			agregarACasillasGanadoras(movimiento);
		}	
	}
	
	private static boolean esCasillaSolucion(int fila, int columna)
	{
		Point movimiento = new Point(fila, columna);
		return movimientosGanadores.contains(movimiento);
	}
	
	private static void quitarDeCasillasGanadoras(Point movimiento)
	{
		movimientosGanadores.remove(movimiento);
	}
	
	private static void agregarACasillasGanadoras(Point movimiento)
	{
		movimientosGanadores.add(movimiento);
	}
	
	public static void clickSobreCasilla(int fila, int columna) 
	{
		if(!getEstaTerminado())
		{
			cambiarLuces(fila, columna);
			actualizarEstadoJuego();
		}
	}
	
	private static void cambiarLuces(int fila, int columna) {
		cambiarLucesFila(fila);
		cambiarLucesColumna(columna);
		cambiarLuzPosicion(fila, columna);		
	}

	private static void cambiarLucesFila(int fila) 
	{
		for (int col = 0; col < matriz.length; col++ ) 
		{
			cambiarLuzPosicion(fila, col);
		}
	}
	
	private static void cambiarLucesColumna(int columna) 
	{
		for (int fila = 0; fila < matriz.length; fila++ ) 
		{
			cambiarLuzPosicion(fila, columna);
		}
		
	}
	private static void cambiarLuzPosicion(int fila, int columna) 
	{
		if (obtenerValor(fila, columna) == 1) 
		{
			cambiarValor(fila, columna, 0);
		}
		else 
		{
			cambiarValor(fila, columna, 1);
		}
	}
	
	private static void actualizarEstadoJuego() 
	{
		setEstaTerminado(getEstaTerminado() || sonTodosCeros());
    }
	
	private static boolean sonTodosCeros()
	{
		boolean sonTodosCeros = true;
		
		for (int fila = 0; fila < matriz.length; fila++) 
		{
            for (int columna = 0; columna < matriz.length; columna++) 
            {	
            	sonTodosCeros = sonTodosCeros && (matriz[fila][columna] == 0) ;            	
            }
         }
		return sonTodosCeros;
	}
	
	private static int obtenerValor( int fila, int columna) 
	{
		return matriz[fila][columna];
	}
	
	private static void cambiarValor( int fila, int columna, int valor) 
	{
		matriz[fila][columna] = valor;
	}
	
	public static int[][] enviarTableroActualizado(){
		return matriz;
	}
	
	public static HashSet<Point> getMovimientosGanadores()
	{
		return movimientosGanadores;
	}
	
	private static void setEstaTerminado(boolean b)
	{
		estaTerminado = b;
	}
	
	public static boolean getEstaTerminado() {
		return estaTerminado;
	}
}